import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'pixi',
        loadChildren: () => import('./pixi/pixi.module').then(m => m.PixiModule),
    },
    {
        path: 'glify',
        loadChildren: () => import('./glify/glify.module').then(m => m.GlifyModule),
    },
    {
        path: 'tangram',
        loadChildren: () => import('./tangram/tangram.module').then(m => m.TangramModule),
    },
    {
        path: 'pixi-and-glify',
        loadChildren: () => import('./pixi-and-glify/pixi-and-glify.module').then(m => m.PixiAndGlifyModule),
    },
    {
        path: '**',
        redirectTo: 'pixi'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

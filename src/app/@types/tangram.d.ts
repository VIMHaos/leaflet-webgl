declare namespace Tangram {

    interface leafletLayerConfig {
        scene: string;
        attribution?: string;
        modifyScrollWheel?: boolean;
    }

    interface sceneEvents {
        load: () => void;
        warning: () => void;
        error: () => void;
    }

    interface TangramSource {
        type: string;
        url: string;
    }

    interface TangramLoadOption {
        base_path?: string;
        file_type?: string;
    }

    interface TangramDynamicData {
        type: string;
        data: GeoJSON.FeatureCollection<GeoJSON.GeometryObject> | GeoJSON.Feature<GeoJSON.MultiPoint> | GeoJSON.GeometryCollection;
    }

    interface selectionEvents {
        click: () => void;
        hover: () => void;
    }

    interface TangramFeatureTitle {
        coords: {
            key: string;
            x: number;
            y: number;
            z: number;
        };
        generation: number;
        key: string;
        source: string;
        style_zoom: number;
    }

    interface TangramFeature {
        layers: string[];
        properties: any;
        source_layer: string;
        source_name: string;
        title: TangramFeatureTitle
    }

    interface LeafletEvent {
        containerPoint: L.Point;
        latlng: L.LatLng;
        layerPoint: L.Point;
        originalEvent: MouseEvent;
        target: any;
        type: string;
    }

    interface selectionEvent {
        changed: boolean;
        feature: TangramFeature;
        leaflet_event: LeafletEvent;
        pixel: L.Point;
    }

    interface TangramLayerData {
        source: string;
    }

    interface TangramLayerDraw {
        [key: string]: TangramLayer;
    }

    interface TangramLayerFilter {
        [key: string]: any;
    }

    interface TangramLayer {
        data?: TangramLayerData;
        draw?: TangramLayerDraw;
        filter?: TangramLayerFilter;
        enabled?: boolean;
        [key: string]: any;
    }

    interface sceneConfig {
        global?: any;
        cameras?: any;
        fonts?: any;
        layers?: {
            [key: string]: any
        };
        lights?: any;
        scene?: any;
        import?: string[];
        sources?: TangramSource;
        styles?: any;
        textures?: any;
    }

    interface IUpdateConfig {
        rebuild: boolean;
    }

    class leafletLayer {
        constructor(leafletLayerConfig);

        addTo(map: L.Map): this;
        removeFrom(map: L.Map): this;
        setSelectionEvents(selectionEvents, params?): this;

        scene: scene;
        selectionEvent: selectionEvent;
    }

    class scene {
        subscribe(sceneEvents): this;
        updateConfig(IUpdateConfig): this;
        updateConfig(IUpdateConfig?): Promise<void>;
        rebuild(): this
        config: sceneConfig;
        setDataSource(name: string, config: TangramSource | TangramDynamicData): void;
    }
}

declare module 'tangram' {
    export = Tangram;
}

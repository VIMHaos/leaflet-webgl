import 'leaflet-pixi-overlay';
import 'proj4leaflet';

import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'leaflet-webgl';
}

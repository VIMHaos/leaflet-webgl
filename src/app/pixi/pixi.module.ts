import { AiMapModule } from '@ai-maps/ai-map';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PixiComponent } from './pixi.component';
import { PIXI_SERVICES } from './services';

@NgModule({
    declarations: [
        PixiComponent,
    ],
    imports: [
        CommonModule,
        AiMapModule,
        RouterModule.forChild([
            {
                path: '',
                component: PixiComponent
            }
        ])
    ],
    providers: [
        ...PIXI_SERVICES,
    ]
})
export class PixiModule { }

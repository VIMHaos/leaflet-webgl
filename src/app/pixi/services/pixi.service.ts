import { Injectable } from '@angular/core';
import { DropShadowFilter } from '@pixi/filter-drop-shadow';
import * as L from 'leaflet';
import * as _ from 'lodash';
import * as PIXI from 'pixi.js';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GeoJSONDataService } from '../../core/services/geo-json-data/geo-json-data.service';
import { MapBaseService } from '../../core/services/map-base/map-base.service';
import { PixiShapeEvents } from './pixi-overlayer/constants/pixi-shape-events';
import { PixiFeatureGroup } from './pixi-overlayer/pixi-feature-group';
import { PixiOverlayer } from './pixi-overlayer/pixi-overlayer';
import { PixiTextureController } from './pixi-overlayer/pixi-texture-controller';
import { PixiMarker } from './pixi-overlayer/shapes/pixi-marker/pixi-marker';
import { PixiPolygon } from './pixi-overlayer/shapes/pixi-polygon/pixi-polygon';
import { PixiPolyline } from './pixi-overlayer/shapes/pixi-polyline/pixi-polyline';

export interface CustomPixiGraphicsObject extends PIXI.Graphics {
    id: number;
    properties: { [name: string]: any };
}

export interface CustomPixiObject extends PIXI.Sprite {
    id: number;
    properties: { [name: string]: any };
}

// class VectorFeatureLayer {
//     constructor(
//         private position: L.LatLng,
//         private options: {
//             icon: {
//                 texture: PIXI.Texture,
//                 rotation?: number
//             },
//             properties: any,
//         }
//     ) {


//     }

//     bindTooltip() {
//     }

// }

function makeText(length: number, characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
    let result = '';
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

@Injectable({
    providedIn: 'root'
})
export class PixiService {
    private textureController = new PixiTextureController();

    private pixiRenderer: PIXI.Renderer;
    private pixiTicker: PIXI.Ticker;
    private pixiContainer: PIXI.Container;
    private pixiLayer: L.PixiOverlay; /*= (() => {
        let firstDraw = true;
        let prevZoom: number;
        let frame = null;

        return L.pixiOverlay((utils, event) => {
            console.log(event);

            const currentMap: L.Map = utils.getMap();

            const currentZoom = utils.getMap().getZoom();
            if (frame) {
                cancelAnimationFrame(frame);
                frame = null;
            }

            this.pixiRenderer = utils.getRenderer();
            this.pixiContainer = utils.getContainer();

            const invScale = 1 / utils.getScale();

            if (event.type === 'add') {
                this.pixiContainer.removeChildren();

                // this.districtFeatureGroup.addTo(this.pixiContainer);
                // this.riverFeatureGroup.addTo(this.pixiContainer);
                // this.markerFeatureGroup.addTo(this.pixiContainer);
                // this.labelFeatureGroup.addTo(this.pixiContainer);

            }

            if (event.type === 'redraw') {

            }


            if (firstDraw || prevZoom !== currentZoom) {
                this.riverFeatureGroup.redraw();
                if (currentZoom > this.projectionZoom(currentMap)) {
                    this.districtFeatureGroup.redraw();
                    this.markerFeatureGroup.redraw();
                    this.labelFeatureGroup.redraw();
                }
            }

            firstDraw = false;
            prevZoom = currentZoom;

            this.pixiRenderer.render(this.pixiContainer);
        }, new PIXI.Container(), {
            pane: 'pixiLayer',
            doubleBuffering: /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
            // destroyInteractionManager: true
        });
    })();
*/

    private pixiOverlayer: PixiOverlayer;
    private prevTexture: PIXI.Texture;
    private interval;
    private markerFocus;

    private markersLength = 100;

    private districtFeatureGroup: PixiFeatureGroup;
    private riverFeatureGroup: PixiFeatureGroup;
    private markerFeatureGroup: PixiFeatureGroup;
    private labelFeatureGroup: PixiFeatureGroup;

    // this is used to simulate leaflet zoom animation timing:
    // private bezierEasing = BezierEasing(0, 0, 0.25, 1);

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(
        private mapBaseService: MapBaseService,
        private geoJSONDataService: GeoJSONDataService,
    ) {

        this.textureController.setTextureBySVG('marker-circle-one', this.makeSVG('one'));
        this.textureController.setTextureBySVG('marker-circle-two', this.makeSVG('two'));
        this.textureController.setTextureBySVG('marker-circle-three', this.makeSVG('three'));

        this.textureController.setTexture('marker-focus', PIXI.Texture.from('/assets/markers/focus-circle.png'));
        this.textureController.setTexture('text-icon', PIXI.Texture.from('/assets/markers/bicycle.png'));

        // this.loader.add('marker-circle-svg', '/assets/pixi/marker-circle.svg');
        // this.loader.add('marker-svg', '/assets/pixi/marker.svg');
        // this.loader.add('marker', '/assets/markers/circle.png');
        // this.loader.add('marker-focus', '/assets/markers/focus-circle.png');

        PIXI.settings.RESOLUTION = window.devicePixelRatio || 1; // added this


        // add shadow
        const dropShadowFilter = new DropShadowFilter();
        dropShadowFilter.color = 0x000020;
        dropShadowFilter.alpha = 0.3;
        dropShadowFilter.blur = 6;
        dropShadowFilter.distance = 15;

        this.pixiOverlayer = new PixiOverlayer({
            pane: 'pixiLayer',
            doubleBuffering: /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
            // destroyInteractionManager: true
        });

        this.districtFeatureGroup = new PixiFeatureGroup();
        this.riverFeatureGroup = new PixiFeatureGroup();
        this.markerFeatureGroup = new PixiFeatureGroup([dropShadowFilter]);
        this.labelFeatureGroup = new PixiFeatureGroup([dropShadowFilter]);


        this.districtFeatureGroup.addTo(this.pixiOverlayer);
        this.riverFeatureGroup.addTo(this.pixiOverlayer);
        this.markerFeatureGroup.addTo(this.pixiOverlayer);
        this.labelFeatureGroup.addTo(this.pixiOverlayer);

        // pixiOverlayer.registerFeatureGroup(this.districtFeatureGroup);
    }

    private makeSVG(type): string {
        let color = '#c6233c';
        switch (type) {
            case 'two':
                color = '#ffd300';
                break;
            case 'three':
                color = '#008000';
                break;
        }

        const svgObject = `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
            <circle fill="${color}" cx="16" cy="16" r="16"></circle>
            <g transform="scale(1)">
            <path fill="#ffffff" d="M23.265,22.37l-3.293-7.777l-3.225-7.6c-0.279-0.662-1.218-0.662-1.5,0.001l-3.212,7.599l-3.298,7.771 c-0.269,0.635,0.324,1.298,0.985,1.098l6.281-1.896l6.274,1.903C22.938,23.668,23.532,23.006,23.265,22.37z"></path>
            </g>
        </svg>`;

        return svgObject;
    }

    init() {
        combineLatest([
            this.mapBaseService.map$,
            this.geoJSONDataService.geoJSON.event$
        ]).pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(([mapInstance, forkGeoJSON]) => {
            mapInstance.createPane('pixiLayer');
            mapInstance.getPane('pixiLayer').style.zIndex = '500';

            console.log('mapInitialize: ', mapInstance.options.crs.code);

            /*
            const latlng: L.LatLng = new L.LatLng(48.15159, 17.10672);

            const randMarker = _.sample(['one', 'two', 'three']);
            const rotation = this.getRandom(0, 12);

            const markerLayer = new PixiMarker(this.mapBaseService.map.project(latlng, this.mapBaseService.projectionZoom), {
                icon: {
                    texture: this.textureController.getTexture('marker-circle-' + randMarker),
                    rotation
                },
                properties: { lat: latlng.lat, lng: latlng.lng }
            });

            this.markerFeatureGroup.addLayer(markerLayer);*/
            this.districtFeatureGroup.clearLayers();
            this.riverFeatureGroup.clearLayers();
            this.markerFeatureGroup.clearLayers();
            this.labelFeatureGroup.clearLayers();

            forkGeoJSON.districts.features.forEach(district => {
                const geometry: number[][] = district.geometry.coordinates[0].map(coords => {
                    const { x, y } = this.mapBaseService.map.project(
                        new L.LatLng(coords[1], coords[0]),
                        this.mapBaseService.projectionZoom
                    );
                    return [x, y];
                });

                const poxiPolygon = new PixiPolygon(geometry, {
                    fill: {
                        color: _.sample([0xc6233c, 0xffd300, 0x008000, 0x0073C1]),
                        opacity: 0.15
                    },
                    stroke: {
                        width: 2,
                        color: 0xCCCCCC,
                        opacity: 1
                    },
                    properties: district.properties
                });

                // poxiPolygon.on('click', (e: PIXI.InteractionEvent) => {
                //     // e.stopPropagation();

                //     console.log('Polygon: ' + e.type);

                //     const pixiPoly: PixiPolygon = e.target as PixiPolygon;
                //     console.log(pixiPoly);
                // });
                // poxiPolygon.on('mouseover', _.debounce((e: PIXI.InteractionEvent) => {
                //     console.log('Polygon: ' + e.type);

                //     const pixiPoly: PixiPolygon = e.target as PixiPolygon;
                //     console.log(pixiPoly.options.properties);
                // }, 500));

                this.districtFeatureGroup.addLayer(poxiPolygon);
            });

            mapInstance.addLayer(new L.GeoJSON(forkGeoJSON.rivers));

            // forkGeoJSON.rivers.features.forEach(river => {
            //     const geometry = river.geometry.coordinates.map(coords => {
            //         const { x, y } = this.mapBaseService.map.project(
            //             new L.LatLng(coords[1], coords[0]), this.mapBaseService.projectionZoom
            //         );
            //         return [x, y];
            //     });

            //     const pixiPolyline = new PixiPolyline(geometry, {
            //         stroke: {
            //             width: 15,
            //             color: 0x0073C1,
            //             opacity: 0.25
            //         },
            //         properties: river.properties
            //     });

            //     this.riverFeatureGroup.addLayer(pixiPolyline);
            // });


            console.log('set markers: ' + this.markersLength);
            for (let i = 0; i < this.markersLength; i++) {
                const latlng: L.LatLng = new L.LatLng(this.getRandom(46.7, 49.5), this.getRandom(16.2, 21.8));


                const randMarker = _.sample(['one', 'two', 'three']);
                const rotation = this.getRandom(0, 12);


                const pixiMarker = new PixiMarker(
                    this.mapBaseService.map.project(latlng, this.mapBaseService.projectionZoom),
                    {
                        icon: {
                            texture: this.textureController.getTexture('marker-circle-' + randMarker),
                            rotation
                        },
                        properties: { lat: latlng.lat, lng: latlng.lng },
                    }
                ).register(PixiShapeEvents.Click, (event) => {
                    const marker = event.target as PIXI.Sprite;

                    let redraw = false;
                    if (this.markerFocus) {
                        this.markerFocus.texture = this.prevTexture;
                        // this.markerFocus.parent.getChildByName('label').visible = false;
                        this.prevTexture = null;
                        this.markerFocus = null;
                        redraw = true;
                    }

                    if (marker) {
                        this.prevTexture = marker.texture;
                        marker.texture = this.textureController.getTexture('marker-focus');
                        this.markerFocus = marker;
                        redraw = true;

                        const markerParent = marker.parent as PixiMarker;


                        // pixiMarker.getChildByName('label').visible = true;

                        // if (
                        //     markerParent.options &&
                        //     markerParent.options.properties &&
                        //     markerParent.options.properties.lat &&
                        //     markerParent.options.properties.lng
                        // ) {
                        //     console.log(markerParent.options.properties, mapInstance);


                        //     L.popup()
                        //         .setLatLng(new L.LatLng(markerParent.options.properties.lat, markerParent.options.properties.lng))
                        //         .setContent(
                        //             'You clicked the PIXI point at longitude:' + markerParent.options.properties.lng +
                        //             ', latitude:' + markerParent.options.properties.lat
                        //         ).openOn(mapInstance);
                        // }

                        this.markerFeatureGroup.forkRedraw();
                    }
                });
                // .bindTooltip({
                //     textSelected: (marker) => {


                //     },
                //     text: (marker) => {
                //         return [
                //             {
                //                 text: 'gf gfdg fdg '
                //             }
                //         ]
                //     }
                // })
                // const text = makeText(this.getRandom(3, 50));

                // const textSprite = new PixiLabel(new L.Point(0, 0), {
                //     text,
                //     fill: {
                //         color: 0xf8f8f8,
                //         opacity: 0.8
                //     },
                //     // icon: {
                //     //     texture: this.textureController.getTexture('text-icon'),
                //     //     rotation
                //     // },
                // });
                // const textSprite = this.createTextSprite(new L.Point(0, 0));
                // textSprite.visible = false;
                // pixiMarker.addChild(textSprite);

                // const textSprite = this.createTextSprite(projectedPoint);
                // textSprite.visible = false;

                this.markerFeatureGroup.addLayer(pixiMarker);
                // this.labelFeatureGroup.addLayer(textSprite);
            }

            forkGeoJSON.trails.features.forEach(river => {
                const geometry = river.geometry.coordinates.map(coords => {
                    const { x, y } = this.mapBaseService.map.project(
                        new L.LatLng(coords[0], coords[1]), this.mapBaseService.projectionZoom
                    );
                    return [x, y];
                });


                const pixiPolyline = new PixiPolyline(geometry, {
                    stroke: {
                        width: 2,
                        color: 0x0073C1,
                        opacity: 1
                    },
                    properties: river.properties
                });

                this.riverFeatureGroup.addLayer(pixiPolyline);
            });


            // const selectMarker = async (marker: CustomPixiObject) => {
            //     let redraw = false;
            //     if (this.markerFocus) {
            //         this.markerFocus.texture = this.prevTexture;
            //         // this.markerFocus.parent.getChildByName('label').visible = false;
            //         this.prevTexture = null;
            //         this.markerFocus = null;
            //         redraw = true;
            //     }

            //     if (marker) {
            //         this.prevTexture = marker.texture;
            //         marker.texture = this.textureController.getTexture('marker-focus');
            //         this.markerFocus = marker;
            //         redraw = true;

            //         // marker.parent.getChildByName('label').visible = true;

            //         if (marker.properties && marker.properties.lat && marker.properties.lng) {
            //             // console.log(marker.properties);

            //             // L.popup()
            //             //     .setLatLng(new L.LatLng(marker.properties.lat, marker.properties.lng))
            //             //     .setContent(
            //             //         'You clicked the PIXI point at longitude:' + marker.properties.lng +
            //             //         ', latitude:' + marker.properties.lat
            //             //     ).openOn(map);
            //         }
            //     }

            //     if (redraw) {
            //         this.pixiRenderer.render(this.pixiContainer);
            //     }
            // };

            // mapInstance.on('click', (e: L.LeafletMouseEvent) => {
            //     // not really nice but much better than before
            //     // good starting point for improvements
            //     const interaction = this.pixiRenderer.plugins.interaction;
            //     const pointerEvent = e.originalEvent;
            //     const pixiPoint = new PIXI.Point();
            //     // get global click position in pixiPoint:
            //     interaction.mapPositionToPoint(pixiPoint, pointerEvent.clientX, pointerEvent.clientY);
            //     // get what is below the click if any:

            //     selectMarker(interaction.hitTest(pixiPoint, this.pixiContainer));
            // });


            // if (this.interval) {
            //     clearInterval(this.interval);
            // }
            // this.interval = setInterval(() => {
            //     const layers = this.markerFeatureGroup.getLayers();
            //     layers.map(layer => {
            //         const latlng: L.LatLng = new L.LatLng(this.getRandom(46.7, 49.5), this.getRandom(16.2, 21.8));
            //         const coords = this.pixiOverlayer.pixiLayer.utils.latLngToLayerPoint(latlng);

            //         layer.position.set(coords.x, coords.y);
            //     });
            //     this.markerFeatureGroup.forkRedraw();
            // }, 1000);

            // if (!this.pixiTicker) {
            //     this.pixiTicker = new PIXI.Ticker();
            //     this.pixiTicker.add((delta) => this.pixiOverlayer.pixiLayer.redraw({ type: 'redraw', delta }));
            // } else {
            //     this.pixiTicker.stop();
            // }

            // this.pixiTicker.start();

            // mapInstance.on('zoomstart', () => this.pixiTicker.stop());
            // mapInstance.on('zoomend', () => this.pixiTicker.start());
            // mapInstance.on('zoomanim', this.pixiOverlayer.pixiLayer.redraw, this.pixiOverlayer.pixiLayer);


            this.pixiOverlayer.pixiLayer.addTo(mapInstance);

            // });
        });
    }

    private getRandom(min: number, max: number): number {
        return min + Math.random() * (max - min);
    }

    private removePIXILayer() {
        this.riverFeatureGroup.clearLayers();
        this.districtFeatureGroup.clearLayers();
        this.markerFeatureGroup.clearLayers();
        this.labelFeatureGroup.clearLayers();

        this.mapBaseService.map$.pipe(takeUntil(this.ngUnsubscribe)).subscribe((map) => {
            this.pixiOverlayer.pixiLayer.removeFrom(map);
        });
    }

    public destroy() {
        this.removePIXILayer();

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
        this.ngUnsubscribe = new Subject<void>();
    }
}

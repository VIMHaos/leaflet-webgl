import * as L from 'leaflet';
import * as PIXI from 'pixi.js';

/**
 * Type definitions for leaflet-pixi-overlay
 * Definitions by: Vasyl Mazur <https://github.com/VIMHaos>
 */

declare module 'leaflet' {
    interface PixiOverlay {
        new(
            callback: (utils: L.PixiOverlay.Utils, event: L.PixiOverlay.PixiOverlayEvent | any) => void,
            pixiContainer: PIXI.Container,
            options?: PixiOverlay.Options
        ): PixiOverlay;

        options: PixiOverlay.Options;
        utils: PixiOverlay.Utils;

        addTo(map: L.Map): PixiOverlay;
        removeFrom(map: L.Map): PixiOverlay;

        redraw(event: PixiOverlay.PixiOverlayEvent | any): void;
    }

    namespace PixiOverlay {
        interface Utils {
            latLngToLayerPoint(wgsOrigin: L.LatLng | [number, number], zoom?: number): L.Point;
            layerPointToLatLng(point: L.Point, zoom?: number): L.LatLng;
            getScale(zoom?: number): number;
            getRenderer(): PIXI.Renderer;
            getContainer(): PIXI.Container;
            getMap(): L.Map;
        }

        type AddEvent = 'add';
        type MoveEvent = 'moveend';
        type ZoomAnimEvent = 'redraw';
        type RedrawEvent = 'redraw';

        type EventType = AddEvent | MoveEvent | ZoomAnimEvent | RedrawEvent;

        interface PixiOverlayEvent extends L.LeafletEvent {
            type: EventType;
            delta?: number;
        }

        interface Options {
            /** 
             * padding
             * (number; defaults to 0.1) How much to extend the drawing area around the map view (relative to its size).
             */
            padding?: number;

            /**
             * forceCanvas
             * (bool; defaults to false) Force use of a 2d-canvas for rendering.
             */
            forceCanvas?: boolean;

            /**
             * doubleBuffering
             * (bool; default to false) Activate double buffering to prevent flickering when refreshing display on some devices (especially iOS devices).
             * This field is ignored if rendering is done with 2d-canvas.
             */
            doubleBuffering?: boolean;

            /**
             * resolution
             * (number; defaults to 2 on retina devices and 1 elsewhere) Resolution of the renderer.
             */
            resolution?: number;

            /** 
             * pane
             * (string; defaults to 'overlayPane') The Leaflet pane where the overlay container is inserted.
             */
            pane?: string;

            /** 
             * destroyInteractionManager
             * (bool; defaults to false) Destroy PIXI Interaction Manager. This is useful when you do not need to use PIXI interaction.
             */
            destroyInteractionManager?: boolean;

            /** 
             * autoPreventDefault
             * (bool; defaults to true) Customize PIXI Interaction Manager autoPreventDefault property. 
             * This option is ignored if destroyInteractionManager is true. 
             * This should be set to false in most situations to let all dom events flow from PIXI to leaflet but it is set by default for compatibility reason.
             */
            autoPreventDefault?: boolean;

            /** 
             * preserveDrawingBuffer
             * (bool; defaults to false) Enables drawing buffer preservation, enable this if you need to call toDataUrl on the webgl context.
             */
            preserveDrawingBuffer?: boolean;

            /** 
             * clearBeforeRender
             * (bool; defaults to true) This sets if the renderer will clear the canvas or not before the new render pass.
             */
            clearBeforeRender?: boolean;

            /**
             * projectionZoom
             * (function(map): Number; defaults to function that returns the average of map.getMinZoom() and map.getMaxZoom()) returns the projection zoom level.
             * Customizing this option can help if you experience visual artifacts.
             */
            projectionZoom?: (map: L.Map) => number;

            /** 
             * shouldRedrawOnMove
             * (function(e: moveEvent): Boolean; defaults to function () {return false;}) Move events trigger a redraw when this function returns true. For example using function (e) {return e.flyTo || e.pinch;} will trigger redraws during flyTo animation and pinch zooms.
             */
            shouldRedrawOnMove?: (e?: any) => boolean;
        }
    }

    function pixiOverlay(
        callback: (utils: L.PixiOverlay.Utils, event: PixiOverlay.PixiOverlayEvent | any) => void,
        pixiContainer: PIXI.Container,
        options?: PixiOverlay.Options,
    ): PixiOverlay;
}
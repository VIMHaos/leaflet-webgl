import { DropShadowFilter } from '@pixi/filter-drop-shadow';
import * as L from 'leaflet';
import * as _ from 'lodash';
import { Container } from 'pixi.js';

import { PixiLayer } from './interfaces/pixi-layer';
import { PixiOverlayer } from './pixi-overlayer';

export class PixiFeatureGroup {
    private layersContainer = new Container();
    private layersDictionary: { [name: string]: PixiLayer } = {};
    private parentOverlayer: PixiOverlayer;

    constructor(filters?: DropShadowFilter[]) {
        if (filters) {
            this.layersContainer.filters = filters;
        }
    }

    addTo(overlayer: PixiOverlayer) {
        this.parentOverlayer = overlayer;
        overlayer.registerFeatureGroup(this);

        overlayer.pixiContainer.addChild(this.layersContainer);

        return this;
    }

    removeFrom(overlayer: PixiOverlayer) {
        overlayer.pixiContainer.removeChild(this.layersContainer);
        this.getLayers().forEach((layer) => {
            // layer.clearEvents()
        });
        overlayer.unregisterFeatureGroup(this);

        return this;
    }

    addLayer(layer: PixiLayer) {
        const id = this.getLayerId(layer);

        this.layersDictionary[id] = layer;

        if (this.layersContainer) {
            this.layersContainer.addChild(layer);
        }

        return this;
    }

    removeLayer(layer: any) {
        const id = layer in this.layersDictionary ? layer : this.getLayerId(layer);

        if (this.layersContainer && this.layersDictionary[id]) {
            this.layersContainer.removeChild(this.layersDictionary[id]);
        }

        delete this.layersDictionary[id];

        return this;
    }

    clearLayers() {
        this.layersContainer.removeChildren();
        this.layersDictionary = {};

        return this;
    }

    getLayerById(id: number) {
        return this.layersDictionary[id] || null;
    }

    getLayers(): PixiLayer[] {
        return _.values(this.layersDictionary);
    }

    getLayerId(layer: PixiLayer) {
        return L.Util.stamp(layer);
    }

    redraw() {
        this.redrawLayers();

        // if (this.condition && this.condition()) {

        //   this.redrawLayers()

        // }

        // if (!this.condition) {
        //   this.rrr()
        // }

    }

    forkRedraw() {
        this.redrawLayers();

        this.parentOverlayer.pixiRenderer.render(this.parentOverlayer.pixiContainer);
    }

    private redrawLayers() {
        this.getLayers().forEach(layer => layer.redraw(this.parentOverlayer.invScale));
    }
}

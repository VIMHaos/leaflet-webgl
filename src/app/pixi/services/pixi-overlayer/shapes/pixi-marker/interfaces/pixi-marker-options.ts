import { PixiFill, PixiIcon, PixiStroke } from '../../../interfaces';

export interface PixiMarkerOptions<P = any> {
    icon?: PixiIcon;
    fill?: PixiFill;
    stroke?: PixiStroke;
    properties?: P;
}

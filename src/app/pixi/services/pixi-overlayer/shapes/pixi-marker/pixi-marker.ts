import { Graphics, Sprite } from 'pixi.js';

import { PixiShapeEventHandler, PixiShapeEvents } from '../../constants/pixi-shape-events';
import { PixiShape } from '../../interfaces/pixi-shape';
import { PixiMarkerOptions } from './interfaces/pixi-marker-options';

export class PixiMarker<P = any> extends Graphics implements PixiShape {

    private clickEvents: PixiShapeEventHandler[] = [];
    private hoverEvents: PixiShapeEventHandler[] = [];

    private tooltip;

    constructor(
        private point: L.Point,
        private options: PixiMarkerOptions<P>
    ) {
        super();

        this.draw();
    }

    private draw() {
        if (this.options.fill) {
            this.beginFill(this.options.fill.color, this.options.fill?.opacity);
        }
        if (this.options.stroke) {
            this.lineStyle(this.options.stroke.width, this.options.stroke?.color, this.options.stroke?.opacity);
        }

        this.drawCircle(0, 0, 30);

        if (this.options.fill) {
            this.endFill();
        }

        if (this.options.icon) {
            const markerSprite = new Sprite(this.options.icon.texture);

            if (this.options.icon.rotation) {
                markerSprite.rotation = this.options.icon.rotation;
            }

            markerSprite.interactive = true;
            markerSprite.buttonMode = true;
            markerSprite.anchor.set(0.5, 0.5);

            markerSprite.on('click', (event) => {
                if (this.clickEvents && this.clickEvents.length) {
                    this.clickEvents.forEach((clickEvent) => {
                        if (clickEvent) {
                            clickEvent(event);
                        }
                    });
                }
            });

            markerSprite.on('mouseover', (event) => {

                if (this.tooltip && this.tooltip.textHover) {
                    const textSprite = this.tooltip.text();
                    textSprite.visible = false;
                    this.addChild(textSprite);
                }


            });

            markerSprite.on('mouseout', (event) => {
                if (this.tooltip) {
                    // remove
                }
            });
            // markerSprite.scale.set(0.75);

            // const textSprite = this.createTextSprite();
            // textSprite.scale.set(0.75);

            // textSprite.visible = false;

            this.addChild(markerSprite);
        }

        this.position.set(this.point.x, this.point.y);
    }

    register(event: PixiShapeEvents, handler: PixiShapeEventHandler) {
        switch (event) {
            case PixiShapeEvents.Hover:
                this.hoverEvents.push(handler);
                break;
            case PixiShapeEvents.Click:
                this.clickEvents.push(handler);
                break;
        }
        return this;
    }

    bindTooltip(data: any) {
        this.tooltip = data;
        return this;
    }
    showTooltip(data: any) {
        this.tooltip = data;
        return this;
    }

    // unregister(event: VectorLayerEvents, handler: MapVectorEventHandler) {
    //   switch (event) {
    //     case VectorLayerEvents.Hover:
    //       this.hoverEvents = this.hoverEvents.filter((h) => h !== handler);
    //       break;
    //     case VectorLayerEvents.Click:
    //       this.clickEvents = this.clickEvents.filter((h) => h !== handler);
    //       break;
    //   }
    //   return this

    // }

    // clearEvents() {
    //   this.clickEvents= [];
    //   this.hoverEvents = [];
    // }

    redraw(scale: number) {
        this.scale.set(scale);
    }
}

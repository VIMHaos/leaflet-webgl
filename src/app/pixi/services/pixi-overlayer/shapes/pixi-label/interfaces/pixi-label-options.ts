import { PixiFill, PixiIcon, PixiStroke } from '../../../interfaces';

export interface PixiLabelOptions {
    text?: string;
    fill?: PixiFill;
    stroke?: PixiStroke;
    icon?: PixiIcon;
}

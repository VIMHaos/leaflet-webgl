import { Graphics, Point, Sprite, Text, TextStyle } from 'pixi.js';

import { PixiShape } from '../../interfaces/pixi-shape';
import { PixiLabelOptions } from './interfaces/pixi-label-options';

export class PixiLabel extends Graphics implements PixiShape {

    constructor(
        private point: L.Point,
        private options: PixiLabelOptions
    ) {
        super();

        this.draw();
    }

    private draw() {
        if (!this.options.text) {
            return null;
        }

        const label = {
            maxWidth: 240,
            minHeight: 32,
            textIndent: this.options.icon ? 40 : 5,
            textPadding: 5,
            fontSize: 12,
            lineHeight: 16
        };

        // create text Sprite
        const textStyle = new TextStyle({
            fontSize: label.fontSize,
            wordWrap: true,
            breakWords: true,
            wordWrapWidth: label.maxWidth,
            lineHeight: label.lineHeight,
        });
        const textSprite = new Text(this.options.text, textStyle);

        // create icon Sprite
        if (this.options.icon) {
            const iconSprite = new Sprite(this.options.icon.texture);
            iconSprite.position.set(label.textPadding);

            this.addChild(iconSprite, textSprite);
        }

        const labelWidth = textSprite.width + label.textIndent + label.textPadding;
        const labelHeight = (textSprite.height > label.minHeight ? textSprite.height : label.minHeight) + (label.textPadding * 2);
        const labelHelfWidth = labelWidth / 2;

        textSprite.position.set(
            label.textIndent,
            textSprite.height < labelHeight ? (labelHeight - textSprite.height) / 2 : 0
        );

        const points = [
            [0, 0],
            [labelWidth, 0],
            [labelWidth, labelHeight],
            [labelHelfWidth + 10, labelHeight],
            [labelHelfWidth, labelHeight + 10],
            [labelHelfWidth - 10, labelHeight],
            [0, labelHeight],
        ];

        // console.log(points);

        if (this.options.fill) {
            this.beginFill(this.options.fill.color, this.options.fill?.opacity);
        }
        if (this.options.stroke) {
            this.lineStyle(this.options.stroke.width, this.options.stroke?.color, this.options.stroke?.opacity);
        }

        this.drawPolygon(points.map(point => new Point(point[0], point[1])));

        if (this.options.fill) {
            this.endFill();
        }

        this.name = 'label';
        this.interactive = false;
        this.buttonMode = false;

        this.position.set(this.point.x, this.point.y);
        this.pivot.set(labelHelfWidth, this.height + 20);

        this.addChild(textSprite);
    }

    redraw(scale: number) {
        this.scale.set(scale);
    }
}

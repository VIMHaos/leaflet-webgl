import { PixiFill, PixiStroke } from '../../../interfaces';

export interface PixiPolygonOptions<P = any> {
    fill?: PixiFill;
    stroke?: PixiStroke;
    properties?: P;
}

import { Graphics, Point } from 'pixi.js';

import { PixiShape } from '../../interfaces/pixi-shape';
import { PixiPolygonOptions } from './interfaces/pixi-polygon-options';

export class PixiPolygon<P = any> extends Graphics implements PixiShape {

    private defaultOptions: PixiPolygonOptions;

    constructor(
        private points: number[][],
        private options: PixiPolygonOptions<P>
    ) {
        super();

        this.defaultOptions = JSON.parse(JSON.stringify(options));

        this.draw();
    }

    getPoints(): number[][] {
        return this.points || [];
    }

    getOptions(): PixiPolygonOptions {
        return this.options;
    }

    private draw() {
        const points = this.getPoints();

        if (this.options.fill) {
            this.beginFill(this.options.fill.color, this.options.fill?.opacity);
        }
        if (this.options.stroke) {
            this.lineStyle(this.options.stroke.width, this.options.stroke?.color, this.options.stroke?.opacity);
        }

        this.drawPolygon(points.map(point => new Point(point[0], point[1])));

        if (this.options.fill) {
            this.endFill();
        }

        this.interactive = true;
        this.buttonMode = true;

        // this.on('click', (e: InteractionEvent) => {
        //     // e.stopPropagation();

        //     console.log('Polygon: ' + e.type);

        //     const pixiPoly: PixiPolygon = e.target as PixiPolygon;
        //     console.log(pixiPoly.options.properties);
        // });
        // this.on('mouseover', e => {
        //     console.log('Polygon: ' + e.type);

        //     const pixiPoly: PixiPolygon = e.target as PixiPolygon;
        //     console.log(pixiPoly.options.properties);
        // });
    }

    redraw(scale: number) {
        if (this.defaultOptions.stroke) {
            this.options.stroke.width = this.defaultOptions.stroke.width * scale;
        }

        this.clear();
        this.draw();
    }

    clearEvents() {
    }
}

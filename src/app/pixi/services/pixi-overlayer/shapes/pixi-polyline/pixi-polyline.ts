import { Graphics } from 'pixi.js';

import { PixiShape } from '../../interfaces/pixi-shape';
import { PixiPolylineOptions } from './interfaces/pixi-polyline-options';

export class PixiPolyline<P = any> extends Graphics implements PixiShape {

    private defaultOptions: PixiPolylineOptions;

    constructor(
        private points: number[][],
        private options: PixiPolylineOptions<P>
    ) {
        super();

        this.defaultOptions = JSON.parse(JSON.stringify(options));

        this.draw();
    }

    getPoints(): number[][] {
        return this.points || [];
    }

    getOptions(): PixiPolylineOptions {
        return this.options;
    }

    private draw() {
        if (this.options.stroke) {
            this.lineStyle(this.options.stroke.width, this.options.stroke?.color, this.options.stroke?.opacity);
        }

        this.points.forEach((point, index) => {
            const [x, y] = point;
            if (0 === index) {
                this.moveTo(x, y);
            } else {
                this.lineTo(x, y);
            }
        });
    }

    redraw(scale: number) {
        if (this.defaultOptions.stroke) {
            this.options.stroke.width = this.defaultOptions.stroke.width * scale;
        }

        this.clear();
        this.draw();
    }

    clearEvents() {
    }
}

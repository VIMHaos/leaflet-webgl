import { PixiStroke } from '../../../interfaces';

export interface PixiPolylineOptions<P = any> {
    stroke?: PixiStroke;
    properties?: P;
}

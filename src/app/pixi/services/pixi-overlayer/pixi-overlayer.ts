import * as L from 'leaflet';
import * as _ from 'lodash';
import { Container, Renderer, Ticker } from 'pixi.js';

import { PixiFeatureGroup } from './pixi-feature-group';

export class PixiOverlayer {
    public pixiRenderer: Renderer;
    public pixiContainer: Container = new Container();
    public pixiLayer: L.PixiOverlay;

    private pixiTicker: Ticker;

    public map: L.Map;
    public currentZoom: number;
    public invScale: number;

    private featureGroups: PixiFeatureGroup[] = [];

    private firstDraw = true;
    private prevZoom: number;

    constructor(
        // pixiOverlayCallback: () => void,
        private options: L.PixiOverlay.Options
    ) {
        this.pixiLayer = this.createPixiOverlay();

        // if (!this.pixiTicker) {
        //     this.pixiTicker = new Ticker();
        //     this.pixiTicker.add((delta) => this.pixiLayer.redraw({ type: 'redraw', delta }));
        // } else {
        //     this.pixiTicker.stop();
        // }

        // this.pixiTicker.start();
    }

    private createPixiOverlay() {
        return L.pixiOverlay((utils, event) => {
            this.pixiRenderer = utils.getRenderer();
            this.pixiContainer = utils.getContainer();

            this.map = utils.getMap();
            this.currentZoom = utils.getMap().getZoom();
            this.invScale = 1 / utils.getScale();

            if (this.firstDraw || this.prevZoom !== this.currentZoom) {
                this.renderFeatureGroups();
            }

            this.firstDraw = false;
            this.prevZoom = this.currentZoom;

            this.pixiRenderer.render(this.pixiContainer);

        }, this.pixiContainer, this.options);
    }

    addTo(mapInstance: L.Map) {
        this.pixiLayer.addTo(mapInstance);

        this.registerEvents(mapInstance);

        return this;
    }

    removeFrom(mapInstance: L.Map) {
        this.pixiLayer.removeFrom(mapInstance);

        this.unRegisterEvents(mapInstance);

        return this;
    }

    registerFeatureGroup(featureGroup: PixiFeatureGroup) {
        this.featureGroups.push(featureGroup);

        return this;
    }

    unregisterFeatureGroup(featureGroup: PixiFeatureGroup) {
        _.remove(this.featureGroups, featureGroup);

        return this;
    }

    private registerEvents(mapInstance: L.Map) {
        mapInstance.on('zoomstart', () => this.pixiTicker.stop());
        mapInstance.on('zoomend', () => this.pixiTicker.start());
        mapInstance.on('zoomanim', this.pixiLayer.redraw, this.pixiLayer);
    }
    private unRegisterEvents(mapInstance: L.Map) {
        mapInstance.on('zoomstart', () => this.pixiTicker.stop());
        mapInstance.on('zoomend', () => this.pixiTicker.start());
        mapInstance.on('zoomanim', this.pixiLayer.redraw, this.pixiLayer);
    }

    private renderFeatureGroups() {
        this.featureGroups.forEach((featureGrouo) => featureGrouo.redraw());
    }
}

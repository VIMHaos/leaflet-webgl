import { BaseTexture, Texture } from 'pixi.js';

export class PixiTextureController {

    private textureDictionary: { [name: string]: Texture } = {};

    setTexture(key: string, texture: Texture): void {
        if (!this.textureDictionary[key]) {
            this.textureDictionary[key] = texture;
        }
    }

    setTextureBySVG(key: string, source: string | HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | BaseTexture): void {
        this.setTexture(key, Texture.from(source));
    }

    getTexture(key: string): Texture {
        return this.textureDictionary[key] || null;
    }

    clear() {
        this.textureDictionary = {};
    }
}

export interface PixiStroke {
    width: number;
    color?: number;
    opacity?: number;
}

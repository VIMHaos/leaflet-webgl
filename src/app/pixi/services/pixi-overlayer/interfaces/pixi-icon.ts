import { Texture } from 'pixi.js';

export interface PixiIcon {
    texture: Texture;
    rotation?: number;
}

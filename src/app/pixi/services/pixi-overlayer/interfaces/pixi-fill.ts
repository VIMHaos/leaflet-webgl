export interface PixiFill {
    color: number;
    opacity?: number;
}

import { PixiMarker } from '../shapes/pixi-marker/pixi-marker';
import { PixiPolygon } from '../shapes/pixi-polygon/pixi-polygon';
import { PixiPolyline } from '../shapes/pixi-polyline/pixi-polyline';

export type PixiLayer = PixiMarker | PixiPolyline | PixiPolygon;

import { InteractionEvent } from 'pixi.js';

export enum PixiShapeEvents {
    Click = 'click',
    Hover = 'hover'
}

export type PixiShapeEventHandler = (event: InteractionEvent) => any;

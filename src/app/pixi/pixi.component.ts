import { Basemap, Basemaps, MapSettings } from '@ai-maps/ai-map/interfaces';
import { Component, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';
import * as _ from 'lodash';

import { BASE_MAPS, BASE_MAPS_KEYS, BASE_MAPS_LIST, MAP_SETTINGS } from '../constants/map-settings';
import { MapBaseService } from '../core/services/map-base/map-base.service';
import { PixiService } from './services/pixi.service';

@Component({
    selector: 'app-pixi',
    templateUrl: './pixi.component.html',
    styleUrls: ['./pixi.component.scss'],
})
export class PixiComponent implements OnInit, OnDestroy {
    public readonly basemapList: Basemap[] = BASE_MAPS_LIST;
    public basemaps: Basemaps = BASE_MAPS;

    public basemapName = _.first(BASE_MAPS_KEYS);
    public mapSettings: MapSettings = Object.assign({}, MAP_SETTINGS);

    constructor(
        private mapBaseService: MapBaseService,
        private pixiService: PixiService,
    ) { }

    ngOnInit(): void {
        this.pixiService.init();
    }

    public mapReady(mapInstance: L.Map) {
        this.mapBaseService.initMap(mapInstance);
        this.mapBaseService.invalidateSize();
    }

    public mapUpdated(mapInstance: L.Map) {
        this.mapBaseService.initMap(mapInstance);
        this.mapBaseService.invalidateSize();
    }

    public changeBasemap(e: Event, basemap: Basemap): void {
        e.stopPropagation();
        if (this.basemapName !== basemap.name) {
            this.basemapName = basemap.name;
        } else {
            this.basemapName = '';
        }
    }

    ngOnDestroy(): void {
        this.pixiService.destroy();
    }
}

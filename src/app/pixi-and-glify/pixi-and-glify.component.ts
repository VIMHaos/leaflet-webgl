import { Basemaps, MapSettings } from '@ai-maps/ai-map/interfaces';
import { Component, OnDestroy, OnInit } from '@angular/core';

import { BASE_MAPS, MAP_SETTINGS } from '../constants/map-settings';
import { MapBaseService } from '../core/services/map-base/map-base.service';
import { GlifyService } from '../glify/services/glify.service';
import { PixiService } from '../pixi/services/pixi.service';
import { TangramService } from '../tangram/services/tangram.service';

@Component({
    selector: 'app-pixi-and-glify',
    templateUrl: './pixi-and-glify.component.html',
    styleUrls: ['./pixi-and-glify.component.scss']
})
export class PixiAndGlifyComponent implements OnInit, OnDestroy {

    public basemaps: Basemaps = BASE_MAPS;
    public basemapName: string = 'osm';
    public mapSettings: MapSettings = Object.assign({}, MAP_SETTINGS);

    constructor(
        private mapBaseService: MapBaseService,
        private glifyService: GlifyService,
        private pixiService: PixiService,
        private tangramService: TangramService,
    ) { }

    ngOnInit(): void { }

    public mapReady(mapInstance: L.Map) {
        this.mapBaseService.initMap(mapInstance);
        this.mapBaseService.invalidateSize();

        this.tangramService.init();
        this.glifyService.init();
        this.pixiService.init();

    }

    ngOnDestroy(): void {
        this.tangramService.destroy();
        this.glifyService.destroy();
        this.pixiService.destroy();
    }

}

import { AiMapModule } from '@ai-maps/ai-map';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GLIFY_SERVICES } from '../glify/services';
import { PIXI_SERVICES } from '../pixi/services';
import { TANGRAM_SERVICES } from '../tangram/services';
import { PixiAndGlifyComponent } from './pixi-and-glify.component';

@NgModule({
    declarations: [
        PixiAndGlifyComponent
    ],
    imports: [
        CommonModule,
        AiMapModule,
        RouterModule.forChild([
            {
                path: '',
                component: PixiAndGlifyComponent
            }
        ])
    ],
    providers: [
        ...PIXI_SERVICES,
        ...GLIFY_SERVICES,
        ...TANGRAM_SERVICES,
    ]
})
export class PixiAndGlifyModule { }

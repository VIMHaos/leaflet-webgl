import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import * as glify from 'leaflet.glify';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GeoJSONDataService } from '../../core/services/geo-json-data/geo-json-data.service';
import { MapBaseService } from '../../core/services/map-base/map-base.service';

@Injectable({
    providedIn: 'root'
})
export class GlifyService {

    private markersLength = 1000000;

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    private glifyLayer;

    constructor(
        private mapBaseService: MapBaseService,
        private geoJSONDataService: GeoJSONDataService,
    ) { }

    init() {
        combineLatest([
            this.mapBaseService.map$,
            this.geoJSONDataService.geoJSON.event$
        ]).pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(([map, geoJSON]) => {

            map.createPane('glifyLayer');
            // let pointCoords: number[][] = [];
            // for (var i = 0; i < this.markersLength; i++) {
            //     pointCoords.push([this.getRandom(47.7, 50), this.getRandom(1.2, 5.8)]);
            // }
            // console.log('pointCoords: ' + pointCoords.length);

            // glify.points({
            //     map: map,
            //     size: (i) => (Math.random() * 17) + 3,
            //     click: (e, point, xy) => {
            //         //set up a standalone popup (use a popup as a layer)
            //         L.popup()
            //             .setLatLng(point)
            //             .setContent(
            // 'You clicked the GLIFY point at longitude:' +
            //     point[glify.longitudeKey] + ', latitude:' + point[glify.latitudeKey]
            //     )
            //             .openOn(map);

            //         console.log(point);
            //     },
            //     data: pointCoords
            // });

            // glify.shapes({
            //     map,
            //     click: (e, feature): void => {
            //         e.originalEvent.preventDefault();
            //         e.originalEvent.stopPropagation();

            //         console.log('You clicked on ' + feature.properties.NAZKR_ENG, feature);
            //     },
            //     data: geoJSON.districts
            // });

            // const groups = _.groupBy(geoJSON.trails.features, (trail) => `${trail.properties.color}-${trail.properties.weight}`);

            // _.values(groups).forEach(group => {
            //     const color = d3.color(group[0].properties.color).rgb();

            //     glify.lines({
            //         map,
            //         latitudeKey: 0,
            //         longitudeKey: 1,
            //         weight: group[0].properties.weight,
            //         color: () => {
            //             return {
            //                 r: color.r,
            //                 g: color.g,
            //                 b: color.b,
            //             };
            //         },
            //         data: {
            //             type: 'FeatureCollection',
            //             features: group
            //         }
            //     });
            // });

            glify.lines({
                map,
                latitudeKey: 1,
                longitudeKey: 0,
                weight: 1,
                color: (item) => {
                    console.log(geoJSON.rivers, item);
                    return {
                        r: 0,
                        g: 0,
                        b: 1,
                    };
                },
                click: (e, feature) => {
                    L.popup()
                        .setLatLng(e.latlng)
                        .setContent('You clicked on ' + feature.properties.name)
                        .openOn(map);

                    // console.log(feature);
                    // console.log(e);
                },
                data: geoJSON.rivers
            });


            // glify.points({
            //     map,
            //     size: 15,
            //     // size: (Math.random() * 17) + 3,
            //     sensitivity: 1,
            //     click: (e, point, xy) => {
            //         e.originalEvent.preventDefault();
            //         e.originalEvent.stopPropagation();

            //         // set up a standalone popup (use a popup as a layer)
            //         L.popup()
            //             .setLatLng(e.latlng)
            //             .setContent(
            //                 'You clicked the GLIFY point at longitude:' +
            //                 point.geometry.coordinates[glify.longitudeKey] + ', latitude:' + point.geometry.coordinates[glify.latitudeKey]
            //             ).openOn(map);
            //     },
            //     data: geoJSON.points
            // });
        });
    }

    private getRandom(min: number, max: number): number {
        return min + Math.random() * (max - min);
    }

    private removeGlifyLayer() {
        if (this.glifyLayer) {
            this.mapBaseService.map$.pipe(takeUntil(this.ngUnsubscribe)).subscribe((map) => {
                this.glifyLayer.removeFrom(map);
            });
        }
    }

    public destroy() {
        this.removeGlifyLayer();

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
        this.ngUnsubscribe = new Subject<void>();
    }
}

import { Basemaps, MapSettings } from '@ai-maps/ai-map/interfaces';
import { Component, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';

import { BASE_MAPS, MAP_SETTINGS } from '../constants/map-settings';
import { MapBaseService } from '../core/services/map-base/map-base.service';
import { GlifyService } from './services/glify.service';

@Component({
    selector: 'app-glify',
    templateUrl: './glify.component.html',
    styleUrls: ['./glify.component.scss']
})
export class GlifyComponent implements OnInit, OnDestroy {
    public basemaps: Basemaps = BASE_MAPS;
    public basemapName: string = 'osm';
    public mapSettings: MapSettings = Object.assign({}, MAP_SETTINGS);

    constructor(
        private mapBaseService: MapBaseService,
        private glifyService: GlifyService,
    ) { }

    ngOnInit(): void { }

    public mapReady(mapInstance: L.Map) {
        this.mapBaseService.initMap(mapInstance);
        this.mapBaseService.invalidateSize();

        this.glifyService.init();
    }

    ngOnDestroy(): void {
        this.glifyService.destroy();
    }
}

import { AiMapModule } from '@ai-maps/ai-map';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GlifyComponent } from './glify.component';
import { GLIFY_SERVICES } from './services';

@NgModule({
    declarations: [
        GlifyComponent,
    ],
    imports: [
        CommonModule,
        AiMapModule,
        RouterModule.forChild([
            {
                path: '',
                component: GlifyComponent
            }
        ])
    ],
    providers: [
        ...GLIFY_SERVICES
    ]
})
export class GlifyModule { }

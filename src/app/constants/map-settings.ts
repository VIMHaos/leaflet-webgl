import { Basemaps, CRS_SYSTEMS, JTSK_DEFINITION, MapSettings, MAX_MAP_ZOOM_JTSK, MIN_MAP_ZOOM_JTSK } from '@ai-maps/ai-map';
import * as L from 'leaflet';
import * as _ from 'lodash';

export const MIN_MAP_ZOOM = 0;
export const MAX_MAP_ZOOM = 20;

// // S-JTSK / Krovak East North definition
export const JTSK_COORDINATE_SYSTEM_DEFINITION = new L.Proj.CRS(CRS_SYSTEMS.JTSK, JTSK_DEFINITION, {
    origin: [-600000.0, -1120000.0],
    resolutions: [
        529.1677250021168,
        264.5838625010584,
        132.2919312505292,
        66.1459656252646,
        26.458386250105836,
        13.229193125052918,
        6.614596562526459,
        2.6458386250105836,
        1.3229193125052918,
        0.5291677250021167,
        0.26458386250105836,
        0.13229193125052918,
        0.06614596562526459
    ]
});

export const MAP_SETTINGS: MapSettings = {
    center: [48.635428, 19.190401],
    zoom: 7,
    minZoom: MIN_MAP_ZOOM,
    maxZoom: MAX_MAP_ZOOM,
    attributionControl: false,
    editable: false,
    zoomAnimation: false,
    zoomControl: {
        show: true,
        position: 'topleft'
    },
    crs: L.CRS.EPSG3857,
    scaleControl: {
        show: true,
        position: 'bottomleft'
    }
};

export const BASE_MAPS: Basemaps = {
    osm: {
        id: 'osm',
        name: 'osm',
        sourceType: 'xyz',
        options: {
            data: 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png',
            minZoom: MIN_MAP_ZOOM,
            maxZoom: MAX_MAP_ZOOM,
            crs: L.CRS.EPSG3857,
            attribution: 'OSM',
            zIndex: 0,
            pane: 'basemappane',
            paneZindex: 200
        },
        grayscale: false
    },
    stamen: {
        id: 'stamen',
        name: 'stamen',
        sourceType: 'xyz',
        options: {
            data: 'http://{s}.sm.mapstack.stamen.com/(toner-background,$fff[difference],$fff[@23],$fff[hsl-saturation@20],toner-lines[destination-in])/{z}/{x}/{y}.png',
            minZoom: MIN_MAP_ZOOM,
            maxZoom: MAX_MAP_ZOOM,
            crs: L.CRS.EPSG3857,
            attribution: 'mapstack.stamen.com',
            zIndex: 0,
        },
        grayscale: false
    },
    'stamen-fastly': {
        id: 'stamen-fastly',
        name: 'stamen-fastly',
        sourceType: 'xyz',

        options: {
            data: '//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            subdomains: 'abcd',
            minZoom: MIN_MAP_ZOOM,
            maxZoom: MAX_MAP_ZOOM,
            crs: L.CRS.EPSG3857,
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
            zIndex: 0,
        },
        grayscale: false
    },
    orto2016: {
        id: 'orto2016',
        name: 'orto2016',
        sourceType: 'xyz',
        options: {
            data: 'https://mpt.svp.sk/server/rest/services/podkladove_mapy/orto_2016/MapServer/tile/{z}/{y}/{x}',
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            minZoom: MIN_MAP_ZOOM_JTSK,
            maxZoom: MAX_MAP_ZOOM_JTSK,
            crs: JTSK_COORDINATE_SYSTEM_DEFINITION,
            zIndex: 0,
        },
        grayscale: false
    },
    orto2019: {
        id: 'orto2019',
        name: 'orto2019',
        sourceType: 'xyz',
        options: {
            data: 'https://mpt.svp.sk/server/rest/services/podkladove_mapy/orto_2019_12v/MapServer/tile/{z}/{y}/{x}',
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            minZoom: MIN_MAP_ZOOM_JTSK,
            maxZoom: MAX_MAP_ZOOM_JTSK,
            crs: JTSK_COORDINATE_SYSTEM_DEFINITION,
            zIndex: 1,
        },
        grayscale: false
    },
};

export const BASE_MAPS_KEYS = _.keys(BASE_MAPS);
export const BASE_MAPS_LIST = _.values(BASE_MAPS);

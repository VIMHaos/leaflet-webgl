import { GeoJSONDataService } from './geo-json-data/geo-json-data.service';
import { MapBaseService } from './map-base/map-base.service';

export const CORE_SERVICES = [
    MapBaseService,
    GeoJSONDataService
];

export interface LayerToggle  {
    name: string;
    visibility: boolean;
}

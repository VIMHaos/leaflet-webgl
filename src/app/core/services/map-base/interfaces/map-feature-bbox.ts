/**
 * bbox for map feature
 *
 * @export
 * @class MapFeatureBbox
 */
export interface MapFeatureBbox {
    minLat: number;
    minLng: number;
    maxLat: number;
    maxLng: number;
}

import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { MapFeatureBbox } from './interfaces/map-feature-bbox';

export class MapBaseService {
    public mapBusySubject = new Subject<boolean>();

    public inProgress$: Observable<boolean>;
    public inProgressSubject = new BehaviorSubject<boolean>(false);

    private mapSubject = new BehaviorSubject<L.Map>(null);
    public map$ = this.mapSubject.asObservable().pipe(filter(i => !!i));

    get map(): L.Map {
        return this.mapSubject.getValue();
    }

    get projectionZoom(): number {
        const mapInstance = this.map;
        return mapInstance ? (mapInstance.getMaxZoom() + mapInstance.getMinZoom()) / 2 : null;
    }

    constructor() {
        this.inProgress$ = this.inProgressSubject.asObservable();
    }

    initMap(mapInstance: L.Map) {
        if (mapInstance) {
            this.mapSubject.next(mapInstance);
        }
    }

    invalidateSize() {
        const mapInstance = this.map;
        if (mapInstance) {
            mapInstance.invalidateSize();
        }
    }

    fitMapByBbox(bbox: MapFeatureBbox) {
        const mapInstance = this.map;
        if (bbox && mapInstance) {
            mapInstance.fitBounds([[bbox.minLat, bbox.minLng], [bbox.maxLat, bbox.maxLng]]);
        }
    }

    setMapView(lat: number, lng: number, zoom: number) {
        const mapInstance = this.map;
        if (mapInstance) {
            mapInstance.setView([lat, lng], zoom);
        }
    }

    zoomOutByZoomLevel(zoomLevel: number) {
        const mapInstance = this.map;
        if (mapInstance) {
            mapInstance.setZoom(mapInstance.getZoom() - zoomLevel);
        }
    }

    zoomInByZoomLevel(zoomLevel: number) {
        const mapInstance = this.map;
        if (mapInstance) {
            mapInstance.setZoom(mapInstance.getZoom() + zoomLevel);
        }
    }

    // funkcia na prezoomovanie ked potrebujeme updatnut mapove vrstvy po zmene
    // tangram si cachuje aktualny zoom a +-1 zoom level
    // overime ci sme na maxime a podla toho vyberieme smer zoomu
    resetZoomByZoomLevel(resetLevel: number, originalZoom: number, zoomType: 'start' | 'end') {
        const mapInstance = this.map;
        if (mapInstance) {

            if (zoomType === 'start') {

                if (originalZoom + resetLevel > mapInstance.getMaxZoom()) {
                    this.zoomOutByZoomLevel(resetLevel);
                } else {
                    this.zoomInByZoomLevel(resetLevel);
                }

            } else {

                if (originalZoom + resetLevel > mapInstance.getMaxZoom()) {
                    this.zoomInByZoomLevel(resetLevel);
                } else {
                    this.zoomOutByZoomLevel(resetLevel);
                }

            }

        }
    }

    clearMap() {
        if (this.map) {
            this.mapSubject.getValue().remove();
            this.mapSubject.next(null);
        }
    }

    setMapBusy(busy: boolean) {
        this.mapBusySubject.next(busy);
    }

}

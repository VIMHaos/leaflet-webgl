import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FeatureCollection, LineString } from 'geojson';
import * as _ from 'lodash';
import { forkJoin, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

import { factoryState } from '../../utils/factory-state';
import { EdgingInputRoute, ForkGEOJSONResponse } from './interfaces/fork-geo-json';

@Injectable({
    providedIn: 'root'
})
export class GeoJSONDataService {
    public geoJSON = factoryState<ForkGEOJSONResponse>(null, [filter(i => !!i)]);

    constructor(
        private http: HttpClient
    ) {
        this.loadGeoData().toPromise();
    }

    public loadGeoData(): Observable<ForkGEOJSONResponse> {
        return forkJoin({
            points: this.http.get<number[][]>('/assets/glify/86T.json').pipe(
                map(points => {
                    // return GeoJSON FeatureCollection
                    return {
                        type: 'FeatureCollection',
                        features: points.slice(0, 50000).map((point: number[]) => {
                            return {
                                type: 'Feature',
                                properties: {},
                                geometry: {
                                    type: 'Point',
                                    coordinates: point
                                }
                            };
                        })
                    };
                }),
            ),
            districts: this.http.get<FeatureCollection>('/assets/glify/CZDistricts.json'),
            rivers: this.http.get<FeatureCollection>('/assets/glify/rivers.json'),
            trails: this.http.get<EdgingInputRoute[]>('/assets/glify/trails.json').pipe(
                map((trails: EdgingInputRoute[]) => {

                    _.orderBy(trails, (i) => i.options.weight);

                    // return GeoJSON FeatureCollection
                    return {
                        type: 'FeatureCollection',
                        features: trails.map((trail: EdgingInputRoute) => {
                            return {
                                type: 'Feature',
                                properties: trail.options,
                                geometry: {
                                    type: 'LineString',
                                    coordinates: trail.latlngs.map(latlng => latlng)
                                }
                            };
                        })
                    } as FeatureCollection<LineString>;
                }),
            ),
        }).pipe(
            tap((forkResp: ForkGEOJSONResponse) => {
                this.geoJSON.set(forkResp);

                console.log('points: ' + forkResp.points.features.length);
                console.log('districts: ' + forkResp.districts.features.length);
                console.log('rivers: ' + forkResp.rivers.features.length);
                console.log('trails: ' + forkResp.trails.features.length);
            })
        );
    }

}

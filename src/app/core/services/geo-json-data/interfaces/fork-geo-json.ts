import { FeatureCollection, LineString, Point, Polygon } from 'geojson';

export interface EdgingRoutesWithInputs {
    inputs: {
        [inputValue: number]: EdgingInputRoute;
    };
}

export interface GeoPoint {
    latlng: [number, number];
    dateTime: Date;
    inputPathsIds?: {
        [key: number]: string;
    };
}

export interface EdgingInputRoute {
    id: string;
    latlngs: [number, number][];
    options: {
        color: string;
        weight: number;
    };
}

export interface EdgingPathRoute {
    id: number;
    geoPoints: GeoPoint[];
    options: {
        color: string;
        weight: number;
    };
}


export interface ForkGEOJSONResponse {
    points: FeatureCollection<Point>;
    districts: FeatureCollection<Polygon>;
    rivers: FeatureCollection<LineString>;
    trails: FeatureCollection<LineString>;
}

import { MonoTypeOperatorFunction } from 'rxjs';

export const operatorsReducer = <T>(...fns: MonoTypeOperatorFunction<T>[]) => (initialVal: any) => fns.reduce((g, f) => f(g), initialVal);

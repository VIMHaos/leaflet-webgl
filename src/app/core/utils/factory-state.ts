import { BehaviorSubject, MonoTypeOperatorFunction, Observable } from 'rxjs';
import { publishReplay, refCount } from 'rxjs/operators';

import { operatorsReducer } from './operators-reducer';

/**
 * Example:
 *
 * // Initialize object
 * const test = factoryState<ITest>(null, [
 *       map(i => {
 *           console.log('map', i);
 *           return i;
 *       }),
 *       delay(1000),
 *       tap(i => {
 *           console.log('tap', i);
 *           return i;
 *       }),
 *       filterExists(),
 *       map(i => {
 *           console.log('map 1', i);
 *           return i;
 *       }),
 *   ]);
 *
 * // subscribe to event
 * test.event$.subscribe(res => console.log('res 2: ' + res));
 *
 * // Set data
 * test.set({ testObject });
 *
 * // get data
 * test.value();
 */

export const factoryState = <T>(defaultState: T, operators: MonoTypeOperatorFunction<T>[] = []) => {
    const event = new BehaviorSubject<T>(defaultState);
    const event$: Observable<T> = event.asObservable().pipe(
        operatorsReducer<T>(
            ...operators,
            ...[
                publishReplay(1),
                refCount()
            ] as MonoTypeOperatorFunction<T>[]
        )
    );

    return {
        set: (newState: T) => {
            event.next(newState);
        },
        value: () => event.getValue(),
        event$
    };
};

import { Injectable } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as Tangram from 'tangram';

import { GeoJSONDataService } from '../../core/services/geo-json-data/geo-json-data.service';
import { ForkGEOJSONResponse } from '../../core/services/geo-json-data/interfaces/fork-geo-json';
import { MapBaseService } from '../../core/services/map-base/map-base.service';

@Injectable({
    providedIn: 'root'
})
export class TangramService {

    private tangramLayer: Tangram.leafletLayer;

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(
        private mapBaseService: MapBaseService,
        private geoJSONDataService: GeoJSONDataService,
    ) { }

    public init() {
        combineLatest([
            this.mapBaseService.map$,
            this.geoJSONDataService.geoJSON.event$
        ]).pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(([map, forkGEOJSON]) => {
            map.createPane('tangramLayer');
            map.getPane('tangramLayer').style.zIndex = '409';

            this.tangramLayer = new Tangram.leafletLayer({
                scene: '/assets/tangram/scene.yaml',
                modifyScrollWheel: false,
                pane: 'tangramLayer',
                events: {
                    // hover: this.tangramOnHover,
                    click: this.tangramOnClick,
                }
            });

            this.tangramLayer.addTo(map);

            this.tangramLayer.scene.subscribe({
                load: () => {
                    // load MVT sources dynamicly from config

                    this.setDataSourceByGeoJSON(forkGEOJSON);
                }
            });
        });
    }

    private setDataSourceByGeoJSON(forkGEOJSON: ForkGEOJSONResponse) {
        const tangramPoints = Object.assign({}, forkGEOJSON.points);

        tangramPoints.features.map(point => {
            point.geometry.coordinates = [point.geometry.coordinates[1], point.geometry.coordinates[0]]
            return point;
        });

        this.tangramLayer.scene.setDataSource('poi', {
            type: 'GeoJSON',
            data: tangramPoints
        });
    }

    private tangramOnHover = (selection) => {
        if (selection.feature) {
            console.log('Hover!', selection);
        }
    }

    private tangramOnClick = (selection) => {
        if (selection.feature) {
            console.log('Click!', selection);
        }
    }

    private removeTangramLayer() {
        if (this.tangramLayer) {
            this.mapBaseService.map$.pipe(takeUntil(this.ngUnsubscribe)).subscribe((map) => {
                this.tangramLayer.removeFrom(map);
            });
        }
    }

    public destroy() {
        this.removeTangramLayer();

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
        this.ngUnsubscribe = new Subject<void>();
    }

    private getRandom(min: number, max: number): number {
        return min + Math.random() * (max - min);
    }
}

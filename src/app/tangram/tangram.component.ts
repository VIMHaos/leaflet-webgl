import { Basemaps, MapSettings } from '@ai-maps/ai-map/interfaces';
import { Component, OnDestroy, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { BASE_MAPS, BASE_MAPS_KEYS, MAP_SETTINGS } from '../constants/map-settings';
import { MapBaseService } from '../core/services/map-base/map-base.service';
import { TangramService } from './services/tangram.service';

@Component({
    selector: 'app-tangram',
    templateUrl: './tangram.component.html',
    styleUrls: ['./tangram.component.scss']
})
export class TangramComponent implements OnInit, OnDestroy {

    public basemaps: Basemaps = BASE_MAPS;
    public basemapName = _.first(BASE_MAPS_KEYS);
    public mapSettings: MapSettings = Object.assign({}, MAP_SETTINGS);

    constructor(
        private mapBaseService: MapBaseService,
        private tangramService: TangramService,
    ) { }

    ngOnInit(): void { }

    public mapReady(mapInstance: L.Map) {
        this.mapBaseService.initMap(mapInstance);
        this.mapBaseService.invalidateSize();

        this.tangramService.init();
    }

    ngOnDestroy(): void {
        this.tangramService.destroy();
    }
}

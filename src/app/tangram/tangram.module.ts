import { AiMapModule } from '@ai-maps/ai-map';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TANGRAM_SERVICES } from './services';
import { TangramComponent } from './tangram.component';



@NgModule({
    declarations: [
        TangramComponent,
    ],
    imports: [
        CommonModule,
        AiMapModule,
        RouterModule.forChild([
            {
                path: '',
                component: TangramComponent
            }
        ])
    ],
    providers: [
        ...TANGRAM_SERVICES,
    ]
})
export class TangramModule { }
